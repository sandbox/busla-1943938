<?php
/**
 * Implementation of hook_rules_action_info().
 * @ingroup rules
 */
function rules_date_difference_rules_action_info() {
  $actions = array(
    'rules_date_difference_action_datediff' => array(
      'label' => t('Subtract two datetime values.'),
      'group' => t('Datetime calculations'),
      'parameter' => array(
        'datetime1' => array(
          'type' => 'date', 
          'label' => t('Earlier date')),
        'datetime2' => array(
          'type' => 'date', 
          'label' => t('Later date')),
      ),
      'provides' => array(
        'datetime_results' => array(
          'type' => 'integer',
          'label' => t('Datetime results'),
          ),
        ),
       ),
    );
  return $actions;
}

function rules_date_difference_action_datediff($earlier, $later) {
  $start_date = new DateObject($earlier);
  $end_date = new DateObject($later);

  $duration = $start_date->difference($end_date, 'years');
  return array ('datetime_results' => $duration);
  
}